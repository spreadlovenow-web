ifndef LANGUAGES
	LANGUAGES := $(shell grep -v "^\#" LANGUAGES)
endif
PO_FILES = $(patsubst %, po/%.po, $(LANGUAGES))
MO_FILES = $(patsubst %, po/%.mo, $(LANGUAGES))
HTML_FILES = $(shell find data -name "*.html")
HTML_FILES_BASE = $(shell for file in $(HTML_FILES); do basename $$file .html; done)
PWD = $(shell pwd)
SITEROOT = http://spreadlovenow.org

all: po/spreadlovenow.pot $(MO_FILES) cleanout $(LANGUAGES) static

static: root/sitemap.xml
	cp -a static out/static
	cp -a root/* out/

root/sitemap.xml:
	mkdir -p root
	echo '<?xml version="1.0" encoding="UTF-8"?>' > $@
	echo '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">' >> $@
	for lang in $(LANGUAGES); \
		do for file in $(HTML_FILES_BASE); \
			do echo "<url><loc>$(SITEROOT)/$$lang/$$file</loc></url>" >> $@; \
		done; \
	done
	echo '</urlset>' >> $@

$(LANGUAGES):
	@if [ ! -e static/img/lovenote_$@.png ]; then echo "cp static/img/lovenote_en.png static/img/lovenote_$@.png"; cp static/img/lovenote_en.png static/img/lovenote_$@.png; fi
	mkdir -p out/$@
	python build.py $@

po/en.po: po/spreadlovenow.pot
	cp $< $@

%.po: po/spreadlovenow.pot
	@if [ ! -e $@ ]; then echo "cp po/spreadlovenow.pot"; cp po/spreadlovenow.pot $@; fi
	msgmerge --no-wrap --update $@ $^

%.mo: %.po
	msgfmt -o $@ $<

po/spreadlovenow.pot: $(HTML_FILES)
	pybabel extract -F pybabel.cfg data -c "TRANSLATORS:" --msgid-bugs-address="i18n-bugs@spreadlovenow.org" -o $@

clean: cleanout
	rm -f po/en.po
	rm -f po/*.mo
	rm -f lighttpd.conf
	rm -f root/sitemap.xml

cleanout:
	rm -rf out.bak
	@if [ -e out ]; then echo "mv out out.bak"; mv out out.bak; fi

update:
	git pull

test: all
	@if [ -e lighttpd.pid ]; then echo "lighttpd still running apparently, run make stoptest first"; exit 1; fi
	sed -e "s!@PWD@!$(PWD)!" lighttpd.conf.in > lighttpd.conf
	lighttpd -f ./lighttpd.conf
	@echo "Test instance started at: http://localhost:5000/"

stoptest:
	if [ -e lighttpd.pid ]; then pid=`cat lighttpd.pid` && kill -TERM $$pid; rm lighttpd.pid; fi

.PHONY: all clean cleanout static update test stoptest
