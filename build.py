#!/usr/bin/python
# coding=utf-8
from mako.template import Template
from gettext import GNUTranslations
from optparse import OptionParser
import sys, os, cgi

LANGUAGES = {
    'da': u'Dansk',
    'en': u'English',
    'fr': u'Français',
    #'nl': u'Nederlands',
    #'de': u'Deutsch',
    #'ru': u'Русский',
    #'ar': u'العربية',
    'pl': u'polski',
    'sq': u'Shqip',
    'el': u'Ελληνικά',
}
MAINTAINERS = {
    'da': [
        u"Joe Hansen <joedalton2@yahoo.dk>",
    ],
    'en': [
        u"Ian Weller <ianweller@gmail.com>",
    ],
    'fr': [
        u"Sam Stuewe <Ch4in3dRe4cti0n@gmail.com>",
    ],
    #'nl': [
    #    u"David Carpenter",
    #],
    #'de': [
    #    u"David Carpenter",
    #],
    #'ru': [
    #    u"David Carpenter",
    #],
    #'ar': [
    #    u"David Carpenter",
    #],
    'pl': [
        u"Piotr Drąg <piotrdrag@gmail.com>",
    ],
    'sq': [
        u"<jshm@programeshqip.org>",
    ],
    'el': [
        u"Dimitris Glezos <glezos@indifex.com>",
    ],
}
RTL_LANGS = ['ar',]

def generate_chooser():
    data = ""
    for langcode in LANGUAGES:
        if langcode in RTL_LANGS:
            langname = '<bdo dir="rtl">%s</bdo>' % LANGUAGES[langcode]
        else:
            langname = LANGUAGES[langcode]
        data += '<li><a href="/%s/@PAGENAME@">%s</a></li>' % (
            langcode, langname
        )
    return data

def generate_credits():
    data = ""
    for langcode in MAINTAINERS:
        if langcode in RTL_LANGS:
            langname = '<bdo dir="rtl">%s</bdo>' % LANGUAGES[langcode]
        else:
            langname = LANGUAGES[langcode]
        data += "<dt>%s</dt>" % langname
        for maint in MAINTAINERS[langcode]:
            data += "<dd>%s</dd>" % cgi.escape(maint)
    return data

def generate_files(lang, chooser, transcredits):
    print "Generating files for %s" % lang
    if lang in RTL_LANGS:
        direction = "rtl"
    else:
        direction = "ltr"
    translations = GNUTranslations(open(os.path.join('po', lang + '.mo')))
    for dirpath, dirnames, filenames in os.walk('data'):
        continue
    for filename in filenames:
        if filename[0] == '.':
            continue
        if filename[-5:] == '.html':
            template = Template(filename=os.path.join('data', filename),
                                input_encoding="utf8")
            output = os.path.join('out', lang, filename)
            fd = file(output, 'w')
            fd.write(template.render_unicode(
                lang=lang,
                direction=direction,
                transcredits=transcredits.encode('ascii', 'xmlcharrefreplace'),
                chooser=chooser.replace(
                    "@PAGENAME@", filename[:-5]
                ).encode('ascii', 'xmlcharrefreplace'),
                _=lambda text: translations.ugettext(text).encode(
                    'ascii', 'xmlcharrefreplace'
                )
            ).encode('utf-8', 'replace'))
            fd.close()

def main():
    parser = OptionParser()
    (options, args) = parser.parse_args()
    if len(args) < 1:
        print "Language codes requried to run"
        sys.exit(1)
    else:
        lang_chooser = generate_chooser()
        transcredits = generate_credits()
        generate_files(args[0], lang_chooser, transcredits)

if __name__ == "__main__":
    main()
